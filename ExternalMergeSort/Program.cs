﻿using Dasync.Collections;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExternalMergeSort
{
    /// <summary>
    /// Sorts a big file.
    /// Inspired by this project : http://www.splinter.com.au/sorting-enormous-files-using-a-c-external-mer/
    /// </summary>
    class Program
    {
        public const long OneGibibyteInBytes = 1073741824L;
        public const string SplitFilename = "split";
        public const string SortedFilename = "sorted";
        const int BufferSize = 4096;

        static async Task Main(string[] args)
        {
            string path = args.Length == 0 ? "./lorem5.txt" : args[0];

            if (!File.Exists(path))
            {

                Console.WriteLine("File not found.");
                Console.ReadKey();
                return;
            }
            Stopwatch sw = Stopwatch.StartNew();
            await Split(path);
            await SortChunksAsyncParallel();
            MergeChunks();
            sw.Stop();
            Console.WriteLine($"Done in {sw.ElapsedMilliseconds}ms.");
            Console.ReadKey();
        }

        static async Task Split(string filePath)
        {
            Console.WriteLine("Splitting...");
            int splitNum = 0;

            StreamWriter sw = new StreamWriter($"./{SplitFilename}{splitNum:d5}.dat");
            using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read, BufferSize, FileOptions.SequentialScan))
            using (StreamReader sr = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize))
            {
                while (sr.Peek() >= 0)
                {
                    // Progress reporting
                    //if (++readLine % 5000 == 0)
                    //{
                    //    Console.WriteLine($"Split: {100.0 * sr.BaseStream.Position / sr.BaseStream.Length:f2}");
                    //}

                    // Copy a line
                    string line = await sr.ReadLineAsync();
                    await sw.WriteLineAsync(line);

                    // If the file is big, then make a new split, except if it's the last line
                    if (sw.BaseStream.Length > 1000000000L && sr.Peek() >= 0)
                    {
                        sw.Close();
                        splitNum++;
                        sw = new StreamWriter($"./{SplitFilename}{splitNum:d5}.dat");
                    }
                }
            }
            sw.Close();
        }

        static async Task SortChunksAsync()
        {
            Console.WriteLine("Sorting...");
            foreach (string path in Directory.GetFiles("./", $"{SplitFilename}*.dat"))
            {
                ConcurrentDictionary<string, long> dictionary = new ConcurrentDictionary<string, long>();
                string[] contents = await File.ReadAllLinesAsync(path);

                // Add each word in the dictionnary
                Parallel.ForEach(contents, line =>
                {
                    // Remove non letter chars
                    line = new string((from c in line
                                       where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c)
                                       select c
                                       ).ToArray());

                    // Iterate through words
                    string[] words = line.Split(' ');
                    foreach (var word in words)
                    {
                        if (!dictionary.TryAdd(word, 1))
                        {
                            dictionary[word] += 1;
                        }
                    }
                });

                // Sort dictionnary by keys
                List<string> keys = dictionary.Keys.ToList();
                keys.Sort();

                // Write the sorted dictionary
                string newPath = path.Replace(SplitFilename, SortedFilename);
                using (StreamWriter sw = new StreamWriter(newPath))
                {
                    for (int i = 0; i < keys.Count; i++)
                    {
                        string key = keys[i];
                        for (int j = 0; j < dictionary[key]; j++)
                        {
                            await sw.WriteLineAsync(key);
                        }
                    }
                }

                // Deleted the unsorted file
                File.Delete(path);

                // Free the dictionary
                dictionary = null;
            }
        }

        static async Task SortChunksAsyncParallel()
        {
            Console.WriteLine("Sorting...");
            await Directory.GetFiles("./", $"{SplitFilename}*.dat").ParallelForEachAsync(async path =>
            {
                ConcurrentDictionary<string, long> dictionary = new ConcurrentDictionary<string, long>();
                string[] contents = await File.ReadAllLinesAsync(path);

                // Add each word in the dictionnary
                Parallel.ForEach(contents, line =>
                {
                    // Remove non letter chars
                    line = new string((from c in line
                                       where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c)
                                       select c
                                       ).ToArray());

                    // Iterate through words
                    string[] words = line.Split(' ');
                    foreach (var word in words)
                    {
                        if (!dictionary.TryAdd(word, 1))
                        {
                            dictionary[word] += 1;
                        }
                    }
                });

                // Sort dictionnary by keys
                List<string> keys = dictionary.Keys.ToList();
                keys.Sort();

                // Write the sorted dictionary
                string newPath = path.Replace(SplitFilename, SortedFilename);
                using (StreamWriter sw = new StreamWriter(newPath))
                {
                    for (int i = 0; i < keys.Count; i++)
                    {
                        string key = keys[i];
                        for (int j = 0; j < dictionary[key]; j++)
                        {
                            await sw.WriteLineAsync(key);
                        }
                    }
                }

                // Deleted the unsorted file
                File.Delete(path);

                // Free the dictionary
                dictionary = null;
            }, 10);
        }

        static void SortChucksParallel()
        {
            Parallel.ForEach(Directory.GetFiles("./", $"{SplitFilename}*.dat"), path =>
            {
                ConcurrentDictionary<string, long> dictionary = new ConcurrentDictionary<string, long>();
                string[] contents = File.ReadAllLines(path);

                // Add each word in the dictionnary
                Parallel.ForEach(contents, line =>
                {
                    // Remove non letter chars
                    line = new string((from c in line
                                       where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c)
                                       select c
                                       ).ToArray());

                    // Iterate through words
                    string[] words = line.Split(' ');
                    foreach (var word in words)
                    {
                        if (!dictionary.TryAdd(word, 1))
                        {
                            dictionary[word] += 1;
                        }
                    }
                });

                // Sort dictionnary by keys
                List<string> keys = dictionary.Keys.ToList();
                keys.Sort();

                // Write the sorted dictionary
                string newPath = path.Replace(SplitFilename, SortedFilename);
                using (StreamWriter sw = new StreamWriter(newPath))
                {
                    for (int i = 0; i < keys.Count; i++)
                    {
                        string key = keys[i];
                        for (int j = 0; j < dictionary[key]; j++)
                        {
                            sw.WriteLine(key);
                        }
                    }
                }

                // Deleted the unsorted file
                File.Delete(path);

                // Free the dictionary
                dictionary = null;
            });
        }

        static void MergeChunks()
        {
            Console.WriteLine("Merging...");
            string[] paths = Directory.GetFiles("./", $"{SortedFilename}*.dat");
            // Number of chunks
            int nbrChunks = paths.Length;
            // Estimated record size
            int recordSize = 100;
            // Max memory usage
            long maxUsage = 7000000000L;
            // Bytes of each queue
            long bufferSize = maxUsage / nbrChunks;
            // Overhead of using Queue<>
            double recordOverhead = 7.5;
            // Number of records in each queue
            int bufferLen = (int)(bufferSize / recordSize / recordOverhead);

            // Open the files
            StreamReader[] readers = new StreamReader[nbrChunks];
            for (int i = 0; i < nbrChunks; i++)
            {
                readers[i] = new StreamReader(paths[i]);
            }

            // Make the queues
            Queue<string>[] queues = new Queue<string>[nbrChunks];
            for (int i = 0; i < nbrChunks; i++)
            {
                queues[i] = new Queue<string>(bufferLen);
            }

            // Load the queues
            for (int i = 0; i < nbrChunks; i++)
            {
                LoadQueue(queues[i], readers[i], bufferLen);
            }

            // Merge
            StreamWriter sw = new StreamWriter("./BigFileSorted.txt");
            bool done = false;
            int lowestIndex;
            int j;
            string lowestValue;
            while (!done)
            {
                // Report the progress
                //if (++progress % 5000 == 0)
                //{
                //    Console.WriteLine($"Merge: {100.0 * progress / records:f2}");
                //}

                // Find the chunk with the lowest value
                lowestIndex = -1;
                lowestValue = "";
                for (j = 0; j < nbrChunks; j++)
                {
                    if (queues[j] != null)
                    {
                        if (lowestIndex < 0 || string.CompareOrdinal(queues[j].Peek(), lowestValue) < 0)
                        {
                            lowestIndex = j;
                            lowestValue = queues[j].Peek();
                        }
                    }
                }

                // If nothing is found, we are done
                if (lowestIndex == -1)
                {
                    done = true;
                    break;
                }

                // Output it
                sw.WriteLine(lowestValue);

                // Remove from the queue
                queues[lowestIndex].Dequeue();

                // Have we emptied the queue? Top it up
                if (queues[lowestIndex].Count == 0)
                {
                    LoadQueue(queues[lowestIndex], readers[lowestIndex], bufferLen);

                    // Was there nothing left to read?
                    if (queues[lowestIndex].Count == 0)
                    {
                        queues[lowestIndex] = null;
                    }
                }
            }

            sw.Close();

            // Close and delete the files
            for (int i = 0; i < nbrChunks; i++)
            {
                readers[i].Close();
                File.Delete(paths[i]);
            }
        }

        static void LoadQueue(Queue<string> queue, StreamReader file, int records)
        {
            for (int i = 0; i < records; i++)
            {
                if (file.Peek() < 0)
                {
                    break;
                }

                queue.Enqueue(file.ReadLine());
            }
        }
    }
}
