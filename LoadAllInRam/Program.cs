﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoadAllInRam
{
    class Program
    {
        const int BufferSize = 4096;
        static readonly ConcurrentDictionary<string, ulong> words = new ConcurrentDictionary<string, ulong>();

        static async Task Main(string[] args)
        {
            string fileName = args[0] == string.Empty ? "./lorem5.txt" : args[0];

            if (!File.Exists(fileName))
            {
                Console.WriteLine("File not found.");
                Console.ReadKey();
                return;
            }

            Stopwatch sw = Stopwatch.StartNew();
            string lines;

            using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read, BufferSize, FileOptions.SequentialScan))
            using (StreamReader sr = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize))
            {
                lines = await sr.ReadToEndAsync();
            }

            IEnumerable<string> splittedLines = CustomSplit(lines, '\n');
            lines = string.Empty;

            Parallel.ForEach(splittedLines, line =>
            {
                // Remove non letter chars
                line = new string(lines.Where(c => char.IsLetter(c) || char.IsSeparator(c)).ToArray());

                // Split the words
                string[] splittedWords = line.Split(' ');

                // Add words in the dictionnary
                foreach (string word in splittedWords)
                {
                    if (!words.TryAdd(word, 1))
                    {
                        words[word] += 1;
                    }
                }
            });

            splittedLines = null;

            await SortAndWriteOutput();
            sw.Stop();
            Console.WriteLine($"Time : {sw.ElapsedMilliseconds}ms");
            Console.ReadKey();
        }

        private static IEnumerable<string> CustomSplit(string newtext, char splitChar)
        {
            var result = new List<string>();
            var sb = new StringBuilder();
            foreach (var c in newtext)
            {
                if (c == splitChar)
                {
                    if (sb.Length > 0)
                    {
                        result.Add(sb.ToString());
                        sb.Clear();
                    }
                    continue;
                }
                sb.Append(c);
            }
            if (sb.Length > 0)
            {
                result.Add(sb.ToString());
            }
            return result;
        }

        static async Task SortAndWriteOutput()
        {
            List<string> keys = words.Keys.ToList();
            keys.Sort();

            // Write words
            using StreamWriter streamWriter = new StreamWriter("./output.txt");
            for (int i = 0; i < keys.Count; i++)
            {
                string key = keys[i];
                for (ulong j = 0; j < words[key]; j++)
                {
                    await streamWriter.WriteLineAsync(key);
                }
            }
        }
    }
}
