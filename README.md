# Big File Sort

J'ai fait plusieurs programmes lors de ce projet avec plus ou moins de succès.

# ReadByteByByte (Programme final)

Programme qui lis le file stream 4096 bytes par 4096 bytes puis trie le tout.
C'est le programme le plus rapide et donc celui que je vous rends.
Pour lancer ce programme, il faut passer le chemin du fichier à trier en paramètre dans une ligne de commande. (Il est possible de faire ça dans les propriété du projet, dans l'onglet debug)

# ReadThenSortDictionary

Premier programme que j'ai fait, c'est le plus lent. Il lis le fichier ligne par ligne de manière asychrone et ajoute les mots dans un dictionnaire qui est ensuite trié.

# ExternalMergeSort

Programme fait en suivant un tutoriel que j'ai adapté. J'ai essayé d'utiliser un maximum les méthodes asynchrones et parallèles. Le programme split le fichier puis les sort. Enfin, il les merges dans un seul fichier.
Cette solution est bien pour ne pas trop utiliser de RAM.

# LoadAllInRam

J'ai essayé de mettre tout le fichier en RAM, mais cela n'as pas maché, le programme ne fonctionne pas.
