﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadByteByByte
{
    class Program
    {
        static readonly ConcurrentDictionary<string, ulong> words = new ConcurrentDictionary<string, ulong>();
        const int BufferSize = 4096;
        const int ReadBufferSize = 1;

        static async Task Main(string[] args)
        {
            string filePath = args[0] == "" ? "./lorem5.txt" : args[0];

            if (!File.Exists(filePath))
            {
                Console.WriteLine("File not found.");
                Console.ReadKey();
                return;
            }

            Console.WriteLine($"Heure de début en ms : {DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond}");
            Console.WriteLine("Reading...");
            Stopwatch sw = Stopwatch.StartNew();
            await using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read, BufferSize, true))
            {
                byte[] buffer = new byte[BufferSize];
                StringBuilder stringBuilder = new StringBuilder();
                int numRead;
                string oldEnd = string.Empty;
                while ((numRead = await fileStream.ReadAsync(buffer, 0, buffer.Length)) != 0)
                {
                    string rawBufferString = Encoding.ASCII.GetString(buffer);
                    StringBuilder bufferString = new StringBuilder();

                    // Remove punctuation
                    foreach (var c in rawBufferString)
                    {
                        if (char.IsLetter(c))
                        {
                            bufferString.Append(c);
                        }
                        else
                        {
                            bufferString.Append(' ');
                        }
                    }
                     
                    // Check the end of the last buffer
                    if (oldEnd != string.Empty)
                    {
                        // The last buffer didn't end with a space
                        if (!char.IsLetter(bufferString[0]))
                        {
                            // This buffer doesn't start with a letter
                            AddWord(oldEnd);
                        }
                        else
                        {
                            // Add the rest of the word
                            StringBuilder choppedWord = new StringBuilder(oldEnd);
                            foreach (char c in bufferString.ToString())
                            {
                                if(c != ' ')
                                {
                                    choppedWord.Append(c);
                                    bufferString.Remove(0, 1);
                                }
                                else
                                {
                                    break;
                                }
                            }

                            oldEnd = choppedWord.ToString();
                            AddWord(choppedWord.ToString());
                        }

                        oldEnd = string.Empty;
                    }

                    string[] splittedBuffer = bufferString.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                    if (bufferString[^1] != ' ')
                    {
                        // This buffer ends with a letter, we have to take care of it
                        oldEnd = splittedBuffer[^1];
                    }

                    // If the buffer ends with a letter, we have to remove the last split
                    int splittedLength = splittedBuffer.Length - (oldEnd != string.Empty ? 1 : 0);
                    Parallel.For(0, splittedLength, i =>
                    {
                        AddWord(splittedBuffer[i]);
                    });
                }
            }

            Console.WriteLine($"Time : {sw.ElapsedMilliseconds}");
            Console.WriteLine("Sorting...");

            // Sort the dictionnary
            List<string> keys = words.Keys.ToList();
            keys.Sort();

            Console.WriteLine($"Time : {sw.ElapsedMilliseconds}");
            Console.WriteLine("Writing...");

            // Write words
            using (StreamWriter streamWriter = new StreamWriter("./output.txt"))
            {
                for (int i = 0; i < keys.Count; i++)
                {
                    string key = keys[i];
                    for (ulong j = 0; j < words[key]; j++)
                    {
                        await streamWriter.WriteLineAsync(key);
                    }
                }
            }

            Console.WriteLine($" Done in {sw.ElapsedMilliseconds}ms");
            Console.WriteLine($"Keys count : {keys.Count}");
            Console.WriteLine($"Heure de fin en ms : {DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond}");
        }

        static void AddWord(string word)
        {
            words.AddOrUpdate(word, 1, (key, oldValue) => oldValue + 1);
        }
    }
}
