﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Collections.Concurrent;

namespace ReadThenSortDictionary
{
    class Program
    {
        static readonly ConcurrentDictionary<string, ulong> words = new ConcurrentDictionary<string, ulong>();
        const int BufferSize = 4096;

        static async Task Main(string[] args)
        {
            string fileName = args[0];

            if (!File.Exists(fileName))
            {
                Console.WriteLine("File not found.");
                Console.ReadKey();
                return;
            }

            Stopwatch sw = Stopwatch.StartNew();
            // Read words
            using(FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read, BufferSize, FileOptions.SequentialScan))
            using (StreamReader sr = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize))
            {
                string line;
                while (sr.Peek() >= 0)
                {
                    // Read line
                    line = await sr.ReadLineAsync();

                    // Remove non letter chars
                    line = new string((from c in line
                                       where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c)
                                       select c
                                       ).ToArray());

                    // Split into words
                    string[] splittedWords = line.Split(' ');

                    // Add words in the dictionnary
                    Parallel.ForEach(splittedWords, word =>
                    {
                        if (!words.TryAdd(word, 1))
                        {
                            words[word] += 1;
                        }
                    });
                }
            }

            await SortAndWriteOutput();

            sw.Stop();
            Console.WriteLine($"Time : {sw.ElapsedMilliseconds}ms");
            Console.ReadKey();
        }

        static async Task SortAndWriteOutput()
        {
            List<string> keys = words.Keys.ToList();
            keys.Sort();

            // Write words
            using (StreamWriter streamWriter = new StreamWriter("./output.txt"))
            {
                for (int i = 0; i < keys.Count; i++)
                {
                    string key = keys[i];
                    for (ulong j = 0; j < words[key]; j++)
                    {
                        await streamWriter.WriteLineAsync(key);
                    }
                }
            }
        }
    }
}
